<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 * @package     local_content
 * @copyright   2003 Cyber Infrastructure Services <abhijeet.k@cisinlabs.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();
require_login();
require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot . '/local/content/lib.php');
require_once($CFG->dirroot . '/local/content/locallib.php');
/**
 * Defines the edit content form.
 * @package     local_content
 * @copyright   2003 Cyber Infrastructure Services <abhijeet.k@cisinlabs.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class edit_content_form extends moodleform {

      /**
       * Defines the form fields.
       */
    public function definition() {
        global $CFG, $DB, $USER;
        $mform  = $this->_form;
        $attributes = array();
        $mform->addElement('header', 'generalhdr', get_string('contentmanager', 'local_content'));
        $mform->addElement('html', '<h2>'.get_string('editcontent', 'local_content').'</h2>');
        $mform->addElement('hidden', 'id', get_string('id', 'local_content'));
        $mform->setDefault('id', null);
        $mform->setType('id', PARAM_NOTAGS);
        $mform->addElement('text', 'title', get_string('title', 'local_content'));
        $mform->setDefault('title', null);
        $mform->setType('title', PARAM_NOTAGS);
        $mform->addRule('title', get_string('required'), 'required', null, null, false, false);
        $mform->addElement('hidden', 'alias', get_string('alias', 'local_content'));
        $mform->setDefault('alias', null);
        $mform->setType('alias', PARAM_NOTAGS);
        $mform->addElement('editor', 'description', get_string('description', 'local_content'), null, array('maxfiles' =>
        EDITOR_UNLIMITED_FILES));
        $mform->addElement('date_selector', 'created', get_string('created', 'local_content'));
        $mform->setType('created', PARAM_NOTAGS);
        $attributes = array();
        $radioarray = array();
        $radioarray[] = $mform->createElement('radio', 'state', '', get_string('yes'), 1, $attributes);
        $radioarray[] = $mform->createElement('radio', 'state', '', get_string('no'), 0, $attributes);
        $mform->addGroup($radioarray, 'state', get_string('state', 'local_content'), array(' '), false);
        $mform->setDefault('state', 1);
        $mform->addElement('text', 'metadescription', get_string('metadescription', 'local_content'));
        $mform->setDefault('metadescription', null);
        $mform->setType('metadescription', PARAM_NOTAGS);
        $mform->addElement('text', 'metakeywords', get_string('metakeywords', 'local_content'));
        $mform->setDefault('metakeywords', null);
        $mform->setType('metakeywords', PARAM_NOTAGS);
        $mform->addElement('text', 'author', get_string('author', 'local_content'));
        $mform->setDefault('author', null);
        $mform->setType('author', PARAM_NOTAGS);
        $option = array();
        $option['Index,Follow'] = get_string('indexfollow' , 'local_content');
        $option['No Index,Follow'] = get_string('noindexfollow' , 'local_content');
        $option['Index,No Follow'] = get_string('indexnofollow' , 'local_content');
        $option['No Index , No Follow'] = get_string('noindexnofollow' , 'local_content');
        $mform->addElement('select', 'robots', get_string('robots', 'local_content'), $option);
        $mform->setDefault('robots', null);
        $mform->setType('robots', PARAM_RAW);
        $this->add_action_buttons();
    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    public function validation($data, $files) {
        global $DB, $CFG;
        $errors = parent::validation($data, $files);
        return $errors;
    }
}