<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 * @package     local_content
 * @copyright   2003 Cyber Infrastructure Services <abhijeet.k@cisinlabs.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// No login check is expected here bacause ... (explain here why anonymous
// internet users should have access to this script).
// @codingStandardsIgnoreLine
require_once('../../config.php');
global $CFG, $USER, $DB, $OUTPUT, $PAGE;
$alias = optional_param('alias', '', PARAM_TEXT);
$html = "";
if ( (string)$alias == null ) {
     redirect($CFG->wwwroot.'/index.php', '404 Not found', null, \core\output\notification::NOTIFY_SUCCESS);
} else {
    require_once($CFG->dirroot . '/local/content/lib.php');
    $alias = stringsafeurl($alias);
    $content = $DB->get_record('local_content', array('alias' => $alias));
    if ( !@$content->id ) {
        redirect($CFG->wwwroot.'/index.php', '404 Not found', null, \core\output\notification::NOTIFY_SUCCESS);
    }
    $PAGE->set_context(context_system::instance());
    $PAGE->set_pagelayout('standard');
    $PAGE->set_title($content->title);
    $PAGE->set_heading($content->title);
    $PAGE->set_url($CFG->wwwroot . '/local/content/page.php');

    $renderer = $PAGE->get_renderer('local_content');
    $url = new moodle_url('/local/content/page.php', array('alias' => $alias));
    if ( $content->metakeywords ) {
        $CFG->additionalhtmlhead .= '<meta name="keywords" content="'.strip_tags($content->metakeywords).'" />';
    }
    if ( $content->metadescription ) {
        $CFG->additionalhtmlhead .= '<meta name="description" content="'.strip_tags($content->metadescription).'" />';
    }
    if ( $content->author ) {
        $CFG->additionalhtmlhead .= '<meta name="author" content="'.strip_tags($content->author).'" />';
    }
    if ( $content->robots ) {
        $CFG->additionalhtmlhead .= '<meta name="robots" content="'.strip_tags($content->robots).'" />';
    }
    echo $OUTPUT->header();
    $PAGE->set_title($content->alias);
    echo $content->description;
    echo $OUTPUT->footer();
}