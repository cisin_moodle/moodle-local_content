moodle-local_content
=======================

includes support for creation on content through moodle.

## Content Local Plugin

This plugin provides content concept for moodle.our lot of client are complaining about this little plugin not available in moodle where admin can manage the content eaisily and update trhe content to regular basis.there is no management of meta keyword and description author and robots in moodle.

To manage these site content management we have written plugin.This plugin will help you to create,edit and delete content inside moodle database.it will be helpful for creating content inside moodle. 

## Installation

1. Unpack the plugin into /local/content within your Moodle install.
2. From the Moodle Administration, expand Site Administration and click "Notifications".

Usage
=======================
1.After installation you can visit the web page through navigation view content.this is for admin access only. this page will redirect to http://www.moodlesite.com/local/content/view.php
2.on this page you can add edit and delete the content available.
3.to view the url you can go to following page.
http://www.yourmoodle.com/local/content/page.php?alias=[alias]

alias is taking from page title.
4.enabling search engine friendly url.
in this you can create .htaccess file and write following rules to create search engine friendly url.

RewriteEngine On
RewriteRule ^content/(.*)$ local/content/page.php?alias=$1&%{QUERY_STRING} [L]

for IIS server please visit
https://docs.microsoft.com/en-us/iis/application-frameworks/install-and-configure-php-applications-on-iis/translate-htaccess-content-to-iis-webconfig



## Copyright

&copy; Cyber Infrastructure Pvt. Ltd  Code for this plugin is licensed under the GPLv3 license.