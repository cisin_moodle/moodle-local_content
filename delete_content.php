<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 * @package     local_content
 * @copyright   2003 Cyber Infrastructure Services <abhijeet.k@cisinlabs.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// No login check is expected here beacause we are check site administrator.
require_once('../../config.php');
require_login();
require_once($CFG->dirroot  .'/local/content/lib.php');
if (!is_siteadmin()) {
    print_error('notauthorize');
}
if (!confirm_sesskey()) {
    print_error('notauthorize');
}
global $DB;
$id = required_param('id', PARAM_INT);
delete_content($id);
redirect(new moodle_url('/local/content/view.php', array()), get_string('delete', 'local_content'));