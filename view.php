<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 * @package     local_content
 * @copyright   2003 Cyber Infrastructure Services <abhijeet.k@cisinlabs.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// No login check is expected here bacause ... (explain here why anonymous
// internet users should have access to this script).
// @codingStandardsIgnoreLine
require_once('../../config.php');
if (!is_siteadmin()) {
    print_error('notauthorize');
}
require_once($CFG->dirroot  .'/local/content/lib.php');
global $CFG;
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title(get_string('content', 'local_content'));
$PAGE->set_heading(get_string('listcontent', 'local_content'));
$PAGE->set_url($CFG->wwwroot . '/local/content/view.php');
require_once($CFG->dirroot . '/local/content/lib.php');
$renderer = $PAGE->get_renderer('local_content');
$urlsearch = new moodle_url('/local/content/view.php');
echo $OUTPUT->header();
echo $renderer->local_content($OUTPUT);
echo $OUTPUT->footer();