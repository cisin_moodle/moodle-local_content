<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 * @package     local_content
 * @copyright   2003 Cyber Infrastructure Services <abhijeet.k@cisinlabs.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
$string['contentmanager'] = 'Content Manager';
$string['addcontent'] = 'Add Content';
$string['content'] = 'Content';
$string['editcontent'] = 'Edit Content';
$string['deletecontent'] = 'Delete Content';
$string['pluginname'] = 'content_manager';
$string['add'] = 'Add content';
$string['listcontent'] = 'View content';
$string['id'] = 'Id';
$string['title'] = 'Title';
$string['alias'] = 'Alias';
$string['description'] = 'Description';
$string['view'] = 'View';
$string['language'] = 'Language';
$string['logo'] = 'Logo';
$string['metadescription'] = 'Meta Description';
$string['metakeywords'] = 'Meta Keywords';
$string['keyrefrence'] = 'Key Refrence';
$string['robots'] = 'Robots';
$string['author'] = 'Author';
$string['edit'] = 'Content edit successfully';
$string['delete'] = 'Delete content successfully';
$string['aliaserror'] = 'Title already exist.Please choose different title';
$string['date'] = 'Date';
$string['created'] = 'Created Time';
$string['state'] = 'Status';
$string['published'] = 'published';
$string['unpublished'] = 'unpublished';
$string['global'] = 'Global';
$string['category'] = 'Category';
$string['timeline'] = 'timeline';
$string['unpublished'] = 'unpublished';
$string['imageavailable'] = 'Image Avialable';
$string['userid'] = 'User Id';
$string['start'] = 'Start';
$string['end'] = 'End';
$string['content_start'] = 'Start';
$string['content_end'] = 'End';
$string['content_group'] = 'content groups';
$string['css'] = 'Custom CSS';
$string['settings'] = 'content Manager Settings';
$string['back'] = 'Back';
$string['settingupdate'] = 'Settings Updated';
$string['newadded'] = 'content Added Successfully';
$string['searchcontent'] = 'Search Content';
$string['indexfollow'] = 'Index,Follow';
$string['noindexfollow'] = 'No Index,Follow';
$string['indexnofollow'] = 'Index,No Follow';
$string['noindexnofollow'] = 'No Index,No Follow';