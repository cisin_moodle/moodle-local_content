<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 * @package     local_content
 * @copyright   2003 Cyber Infrastructure Services <abhijeet.k@cisinlabs.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;
require_once($CFG->libdir . '/outputlib.php');
require_once($CFG->dirroot . '/local/content/lib.php');
require_once($CFG->dirroot.'/lib/filelib.php');

/**
 * The core content renderer class
 * @package     local_content
 * @copyright   2003 Cyber Infrastructure Services <abhijeet.k@cisinlabs.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class local_content_renderer extends plugin_renderer_base {

     /**
      * Render a content
      * @param stdclass $OUTPUT renderer
      * @return string $html
      */
    public function local_content ($OUTPUT) {
        global $CFG, $USER, $DB;
        $html = "";
        $stredit = get_string('edit');
        $strdelete = get_string('delete');
        $table = new html_table();
        $page = optional_param('page', 0, PARAM_INT);
        $perpage = optional_param('perpage', 10, PARAM_INT);
        $querystring = optional_param('querystring', '', PARAM_TEXT);
        $filter = "";
        $totalcount = $DB->get_record_sql('SELECT count(*) as count FROM {local_content} where
        '.$DB->sql_like('LOWER(title)', '?'), array('%'.strtolower($querystring).'%'));
        $count = $totalcount->count;
        $start = $page * $perpage;
        if ( $start > $count ) {
            $page = 0;
            $start = 0;
        }
        $html .= $OUTPUT->heading(get_string('listcontent', 'local_content'));
        $siteurl = new moodle_url('/local/content/view.php');
        $table->head = (array) get_strings(array('id', 'title', 'alias', 'view', 'editcontent'), 'local_content');
        $contents = $DB->get_records_sql('SELECT * FROM {local_content} where '.$DB->sql_like('LOWER(title)',
        '?'),  array('%'.strtolower($querystring).'%'), $start, $perpage);
        $html .= $this->search_local_content($querystring);
        $html .= $OUTPUT->single_button(new moodle_url('/local/content/edit_content.php'), get_string('add', 'local_content'));
        if ( $contents ) {
            foreach ($contents as $content) {
                $url = new moodle_url('/local/content/edit_content.php', array('id' => $content->id));
                $buttons = array();
                $buttons[] = html_writer::link($url, $OUTPUT->pix_icon('t/edit', get_string('edit', 'local_content')));
                $url = new moodle_url('/local/content/delete_content.php', array('id' => $content->id, 'sesskey' => sesskey()));
                $buttons[] = html_writer::link($url, $OUTPUT->pix_icon('t/delete', $strdelete));
                $view = '<a href="' . $CFG->wwwroot. '/local/content/page.php?alias='.$content->alias.'">'
                .get_string('listcontent', 'local_content').'</a>';
                $table->data[] = array(
                    $content->id,
                    '<a href="' . $CFG->wwwroot. '/local/content/page.php?alias='.$content->alias.'">'.$content->title.'</a>',
                    $content->alias,
                    $view,
                    implode(' ', $buttons)
                );
            }
        } else {
            $table->data[] = array('-', '-', '-', '-', '-');
        }
        $html .= html_writer::table($table);
        $html .= $OUTPUT->paging_bar($count, $page, $perpage, $siteurl);
        $html .= $OUTPUT->single_button(new moodle_url('/local/content/edit_content.php') , get_string('add' , 'local_content'));
        return $html;
    }

    /**
     * Renders html to display a article search form
     * @param string $value
     * @return string $html
     */
    public function search_local_content($value = '') {
        $searchurl = new moodle_url('/local/content/view.php');
        $formid = 'contentsearch';
        $formid = 'contentsearchbox';
        $inputid = 'content_search_q';
        $inputsize = 20;
        $strsearchcourses = get_string("searchcontent" , "local_content");
        $output = html_writer::start_tag('form', array('id' => $formid, 'action' => $searchurl, 'method' => 'post'));
        $output .= html_writer::start_tag('fieldset', array('class' => 'coursesearchbox invisiblefieldset'));
        $output .= html_writer::tag('label', $strsearchcourses.': ', array('for' => $inputid));
        $output .= html_writer::empty_tag('input', array('type' => 'text', 'id' => $inputid,
            'size' => $inputsize, 'name' => 'querystring', 'value' => s($value)));
        $output .= html_writer::empty_tag('input', array('type' => 'submit',
            'value' => get_string('go')));
        $output .= html_writer::end_tag('fieldset');
        $output .= html_writer::end_tag('form');

        return $output;
    }
}