<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 * @package     local_content
 * @copyright   2003 Cyber Infrastructure Services <abhijeet.k@cisinlabs.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// No login check is expected here bacause ... (explain here why anonymous
// internet users should have access to this script).
require_once('../../config.php');
require_login();
require_once($CFG->dirroot.'/local/content/edit_content_form.php');
require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot.'/local/content/lib.php');
require_once($CFG->dirroot.'/local/content/locallib.php');
if ( !is_siteadmin() ) {
    print_error('notauthorized');
}
$context = context_system::instance();
$id = optional_param('id', 0, PARAM_INT);
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot .'/local/content/edit_content.php', array('id' => $id));
$editoroptions = page_get_editor_options($context);
$data = new stdClass();
$filemanageroptions = array( 'maxfiles' => 1, 'accepted_types' => array('web_image'));
$draftitemid = file_get_submitted_draft_itemid('files_filemanager');
$mform = new edit_content_form(new moodle_url('/local/content/edit_content.php', array('id' => $id)));
if ( $id ) {
    edit_content($id, $mform, $context, $filemanageroptions);
} else {
    add_content($mform, $draftitemid, $filemanageroptions, $context);
}
$title = get_string('edit', 'local_content');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->navbar->add($title);
echo $OUTPUT->header();
$mform->display();
echo $OUTPUT->footer();