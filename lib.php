<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 * @package     local_content
 * @copyright   2003 Cyber Infrastructure Services <abhijeet.k@cisinlabs.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

// No login check is expected here bacause ... (explain here why anonymous
// internet users should have access to this script).
// @codingStandardsIgnoreLine
/**
 * Adding navigation to menu
 *
 * @param global_navigation $nav navigation object
 * @return null
 */
function local_content_extend_navigation(global_navigation $nav) {
    global $CFG, $DB, $PAGE;
    if ( is_siteadmin() ) {
        $url = $CFG->wwwroot.'/local/content/view.php';
        $pagename = get_string('listcontent', 'local_content');
        $flat = new flat_navigation_node(navigation_node::create($pagename, $url), 0);
        $nav->add_node($flat);
    }
    if ( $PAGE->url->compare(new moodle_url('/local/content/page.php')) ) {
        $alias = optional_param('alias', '', PARAM_TEXT);
        if ( !empty($alias) ) {
            $content = $DB->get_record('local_content', array('alias' => $alias));
            $contentname = $content->title;
            $url = new moodle_url('/local/content/page.php', array('alias' => $alias));
            $flat = new flat_navigation_node(navigation_node::create($contentname, $url), 0);
            $nav->add_node($flat);
        }
    }
}

/**
 * Function to add content
 *
 * @param stdclass $mform
 * @param int $draftitemid
 * @param array $options
 * @param stdclass $context
 * @return null
 */
function add_content($mform, $draftitemid, $options, $context) {
    global $CFG, $DB, $USER;
    $content = new stdClass();
    $content->files_filemanager = $draftitemid;
    if ( $mform->is_cancelled() ) {
        redirect(new moodle_url('/local/content/view.php'));
    } else if ( $data = $mform->get_data() ) {
        $content->title = $data->title;
        $content->description = $data->description['text'];
        $content->metadescription = $data->metadescription;
        $content->metakeywords = $data->metakeywords;
        $content->robots = $data->robots;
        $content->author = $data->author;
        $content->state = $data->state;
        $content->created = $data->created;
        $content->alias = stringsafeurl($content->title);
        $alias = $DB->record_exists('local_content', array( 'alias' => $content->alias));
        if ( $newcontent = $DB->record_exists('local_content', array( 'alias' => $content->alias)) ) {
            redirect(new moodle_url('/local/content/edit_content.php'),
            get_string('aliaserror', 'local_content'), \core\output\notification::NOTIFY_ERROR);
        } else {
            $newcontent = $DB->insert_record('local_content', $content, true);
            redirect(new moodle_url('/local/content/view.php'), get_string('newadded',
            'local_content'), \core\output\notification::NOTIFY_SUCCESS);
        }
    }
}

/**
 * Function to edit content
 *
 * @param int $id
 * @param stdclass $mform
 * @param stdclass $context
 * @param array $options
 * @return null
 */
function edit_content($id, $mform, $context, $options) {
    global $CFG, $DB;
    $content = $DB->get_record('local_content', array('id' => $id));
    $mform->set_data(['description' => ['text' => $content->description]]);
    $mform->set_data($content);
    if ( $mform->is_cancelled() ) {
        redirect(new moodle_url('/local/content/view.php'));
    } else if ( $data = $mform->get_data() ) {
        $content->title = $data->title;
        $content->description = $data->description['text'];
        $content->metadescription = $data->metadescription;
        $content->metakeywords = $data->metakeywords;
        $content->robots = $data->robots;
        $content->author = $data->author;
        $content->state = $data->state;
        $content->created = $data->created;
        $content->id = $data->id;
        $content->alias = stringsafeurl($content->title);
        $predata = $DB->get_record('local_content', array( 'alias' => $content->alias));
        if ( @$predata->id ) {
            if ( $predata->id == $content->id ) {
                $newcontent = $DB->update_record('local_content', $content, true);
                redirect(new moodle_url('/local/content/view.php', array()), get_string('edit', 'local_content'));
            } else {
                redirect(new moodle_url('/local/content/edit_content.php', array( 'id' => $content->id)),
                get_string('aliaserror', 'local_content'), \core\output\notification::NOTIFY_ERROR);
            }
        } else {
            $newcontent = $DB->update_record('local_content', $content, true);
            redirect(new moodle_url('/local/content/view.php', array()), get_string('edit', 'local_content'));
        }
    }
}
/**
 * Function to delete content
 *
 * @param int $id
 * @return null
 */
function delete_content($id) {
    global $DB;
    $content = new stdClass();
    $newcontent = $DB->delete_records('local_content', array ('id' => $id));
}
/**
 * Fetch content
 *
 * @param string $alias
 * @return stdclass $content
 */
function getcontent($alias) {
    global $DB, $CFG;
    if ( (string)$alias == null ) {
        print_error('error');
    } else {
        $content = $DB->get_record('local_content', array('alias' => $alias));
        if ( @$content->id ) {
            return $content;
        } else {
            redirect($CFG->wwwroot.'/index.php', get_string('notfound', 'local_content'), null,
            \core\output\notification::NOTIFY_INFO);
        }
    }
}

/**
 * Safe url for alias
 *
 * @param string $string
 * @return string $string
 */
function stringsafeurl($string) {
    global $DB;
    $str = str_replace('-', ' ', $string);
    $str = preg_replace(array('/\s+/', '/[^A-Za-z0-9\-]/'), array('-', ''), $str);
    $str = trim(strtolower($str));
    return $str;
}